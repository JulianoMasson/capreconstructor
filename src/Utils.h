#pragma once
#include <vtkSmartPointer.h>


class vtkRenderer;
class vtkRenderWindow;
class vtkMatrix4x4;
class vtkAbstractPicker;
class vtkActor;
class vtkTransform;
class vtkPolyData;
class vtkPoints;
class vtkImageData;

class Camera;

class Utils
{
public:
	Utils();
	~Utils();
	static void transformPoint(double point[3], vtkSmartPointer<vtkMatrix4x4> matrixRT);
	//p[0] = x; p[1] = y; p[2] = z;
	static void createDoubleVector(double x, double y, double z, double vector[3])
	{
		if (!vector)
		{
			vector = new double[3];
		}
		vector[0] = x;
		vector[1] = y;
		vector[2] = z;
	}
	static void createDoubleVector(const double xyz[3], double vector[3])
	{
		if (!vector)
		{
			vector = new double[3];
		}
		std::memcpy(vector, xyz, sizeof(double) * 3);
	}
	
	//True if the file exists, false otherwise
	static bool exists(const std::string& name);

	//Get file extesion without the dot
	static std::string getFileExtension(const std::string & filePath);

	static std::string getFileName(const std::string & filePath, bool withExtension = true);

	static std::string getPath(const std::string & filePath, bool returnWithLastSlash = true);

	static std::string toUpper(const std::string & str);

	//Write some T value inside a binary file
	template <class T>
	static void writeBin(std::ofstream &out, T val)
	{
		out.write(reinterpret_cast<char*>(&val), sizeof(T));
	}

	//Read some T value inside a binary file
	template <class T>
	static T readBin(std::ifstream &in)
	{
		T val;
		in.read(reinterpret_cast<char*>(&val), sizeof(T));
		return val;
	}
};