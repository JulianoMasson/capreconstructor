#pragma once
#include <vector>
#include <string>

#include <vtkSmartPointer.h>

#include <AVTexturing.h>

class vtkPolyData;
class Camera;

class HelperAVTexturing
{
public:
	HelperAVTexturing() {};
	~HelperAVTexturing() {};

	static bool computeAVTexturization(vtkSmartPointer<vtkPolyData> inputMesh, const std::string& camerasPath,
		const std::string & imagesFolder, const std::string& outputPath, const AVTexturing::Options& options);

private:
	static void PolyData2MeshAux(vtkSmartPointer<vtkPolyData> inputMesh, AVTexturing::MeshAux& outMesh);
	static void createPointVisibilityData(AVTexturing::MeshAux& mesh, vtkSmartPointer<vtkPolyData> meshPolyData, std::vector<Camera*> cameras);
};


