#pragma once

#include <vector>
#include <vtkSmartPointer.h>

class GPSData;
class vtkImageActor;
class vtkMatrix4x4;
class vtkRenderer;
class vtkTransform;
class vtkActor;
class vtkPolyData;

class Camera
{
private:
	// {focal_x, focal_y}
	float focalDistance[2];
	// {center_x, center_y}
	float principalPoint[2];
	unsigned int width;
	unsigned int height;
	vtkSmartPointer<vtkMatrix4x4> matrixRt = nullptr;
	vtkSmartPointer<vtkMatrix4x4> matrixRtInverted = nullptr;
public:
	Camera();
	Camera(const std::string& filePath, const float focalDistance[2], const float principalPoint[2], 
		const unsigned int width, const unsigned int height, const vtkSmartPointer<vtkMatrix4x4> matrixRt);
	~Camera();

	std::string filePath = "";
	/*
	Calc the camera points:
	origin of the camera = [0]
	[1]--------[2]
	|		    |
	|----[5]----|
	|           |
	[4]--------[3]
	[5]->the distance(z coordinate) is actually 5% less than the others points, to avoid exceed the image with zoon when we do Utils::updateCamera
	*/
	bool calcCameraPoints();

	std::vector<double*> cameraPoints;

	void updateFocalDistance(vtkSmartPointer<vtkRenderer> renderer, float focalX, float focalY);
	void updateMatrixRt(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkMatrix4x4> matrixRt);

	float getFocalX() const { return focalDistance[0]; };
	float getFocalY() const { return focalDistance[1]; };
	float getPrincipalPointX() const { return principalPoint[0]; };
	float getPrincipalPointY() const { return principalPoint[1]; };
	unsigned int getWidth() const { return width; };
	unsigned int getHeight() const { return height; };

	void setMatrixRt(vtkSmartPointer<vtkMatrix4x4> matrixRt);
	vtkSmartPointer<vtkMatrix4x4> getMatrixRt() const;
	
	//Creates a closed frustum with the image plane with the distance to the origin point = dist
	vtkSmartPointer<vtkPolyData> getClosedFrustum(double dist = 0.5);
};