#pragma once
#include <vtkSmartPointer.h>

class vtkPolyData;

class HelperFastQuadricSimplification
{
public:
	HelperFastQuadricSimplification() {};
	~HelperFastQuadricSimplification() {};

	// Define the FastQuadricSimplification options
	struct Options
	{
		Options(const unsigned int& numberOfReductions = 3, const float& percentageOfReduction = 0.5) :
			numberOfReductions(numberOfReductions), percentageOfReduction(percentageOfReduction) {}
		//Parameters
		int numberOfReductions;
		float percentageOfReduction;
	};

	// Return a decimated mesh (reduction is in % of the original mesh polygon count), only holds normals, color and texture coordinates
	static vtkSmartPointer<vtkPolyData> ComputeQuadricDecimation(
		vtkSmartPointer<vtkPolyData> polyData,
		const HelperFastQuadricSimplification::Options& options);
};


