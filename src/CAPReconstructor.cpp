#include <iostream>
#include <string>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkStaticCleanPolyData.h>
#include <vtkVertexGlyphFilter.h>
#include "json.hpp"
#include "PLYReader.h"
#include "PLYWriter.h"
#include "HelperAdaptativeSolvers.h"
#include "HelperFastQuadricSimplification.h"
#include "HelperAVTexturing.h"

std::string DateTime()
{
	time_t now = time(0);
	tm* ltm = localtime(&now);
	return std::to_string(ltm->tm_mday) + "/" +
		std::to_string(1 + ltm->tm_mon) + "/" +
		std::to_string(1900 + ltm->tm_year) + " " +
		std::to_string(ltm->tm_hour) + ":" +
		std::to_string(ltm->tm_min) + ":" +
		std::to_string(ltm->tm_sec) + " ";
}

bool LoadParameters(const std::string& parametersFilename,
	PoissonRecon::Options& poissonParameters,
	SurfaceTrimmer::Options& surfaceTrimmerParameters,
	HelperFastQuadricSimplification::Options& fastQuadricSimplificationParameters,
	AVTexturing::Options& texturizationParameters,
	double& pointCloudFilter_tolerance)
{
	nlohmann::json jsonFile;
	try
	{
		std::ifstream parametersFile(parametersFilename);
		if (!parametersFile.is_open())
		{
			return EXIT_FAILURE;
		}
		jsonFile = nlohmann::json::parse(parametersFile);
	}
	catch (const std::exception&)
	{
		return EXIT_FAILURE;
	}

	pointCloudFilter_tolerance = jsonFile["PointCloudFilter"]["tolerance"];

	poissonParameters.depth = jsonFile["Poisson"]["depth"];
	poissonParameters.boundary = jsonFile["Poisson"]["boundary"];
	poissonParameters.samplesPerNode = jsonFile["Poisson"]["samplesPerNode"];

	surfaceTrimmerParameters.trim_value = jsonFile["SurfaceTrimmer"]["trim"];
	surfaceTrimmerParameters.smooth = jsonFile["SurfaceTrimmer"]["smooth"];
	surfaceTrimmerParameters.aRatio = jsonFile["SurfaceTrimmer"]["aRatio"];

	fastQuadricSimplificationParameters.numberOfReductions =
		jsonFile["FastQuadricSimplification"]["numberOfReductions"];
	fastQuadricSimplificationParameters.percentageOfReduction =
		jsonFile["FastQuadricSimplification"]["percentageOfReductions"];

	texturizationParameters.multiBandDownscale = jsonFile["AVTexturing"]["multiBandDownscale"];
	texturizationParameters.bestScoreThreshold = jsonFile["AVTexturing"]["bestScoreThreshold"];
	texturizationParameters.angleHardThreshold = jsonFile["AVTexturing"]["angleHardThreshold"];
	texturizationParameters.forceVisibleByAllVertices = jsonFile["AVTexturing"]["forceVisibleByAllVertices"];
	texturizationParameters.visibilityRemappingMethod = jsonFile["AVTexturing"]["visibilityRemappingMethod"];
	texturizationParameters.textureSide = jsonFile["AVTexturing"]["textureSide"];
	texturizationParameters.padding = jsonFile["AVTexturing"]["padding"];
	texturizationParameters.downscale = jsonFile["AVTexturing"]["downscale"];
	texturizationParameters.fillHoles = jsonFile["AVTexturing"]["fillHoles"];
	texturizationParameters.useUDIM = jsonFile["AVTexturing"]["useUDIM"];

	return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
	std::cout << "CAPReconstructor " <<
		CAPRECONSTRUCTOR_VERSION_MAJOR << "." <<
		CAPRECONSTRUCTOR_VERSION_MINOR << "." <<
		CAPRECONSTRUCTOR_VERSION_PATCH << std::endl;
	if (argc != 4 && argc != 7)
	{
		std::cout << "Erro ao passar os argumentos" << std::endl;
		std::cout << "Argumentos: <parameters_file> <point_cloud_input> <mesh_output_path> [<cameras_sfm/nvm_input> <project_folder> <textured_mesh_output_path>](Opcionais)" << std::endl;
		return EXIT_FAILURE;
	}
	auto parametersFile = argv[1];
	auto pointCloudInput = argv[2];
	auto meshOutput = argv[3];
	auto camerasInput = "";
	auto projectFolder = "";
	auto texturedMeshOutput = "";
	if (argc == 7)
	{
		camerasInput = argv[4];
		projectFolder = argv[5];
		texturedMeshOutput = argv[6];
	}

	std::cout << "Carregando os par�metros" << std::endl;
	PoissonRecon::Options poissonParameters;
	SurfaceTrimmer::Options surfaceTrimmerParameters;
	HelperFastQuadricSimplification::Options fastQuadricSimplificationParameters;
	AVTexturing::Options texturizationParameters;
	double pointCloudFilter_tolerance;
	if (LoadParameters(parametersFile, poissonParameters, surfaceTrimmerParameters,
		fastQuadricSimplificationParameters, texturizationParameters, pointCloudFilter_tolerance))
	{
		std::cout << "Falha ao carregar o arquivo com os par�metros " << parametersFile << std::endl;
		return EXIT_FAILURE;
	}
	// Read the point cloud
	std::cout << DateTime() << "Carregando a nuvem de pontos" << std::endl;
	PLYReader plyReader;
	auto pointCloud = plyReader.read(pointCloudInput);
	if (pointCloud == nullptr)
	{
		std::cout << DateTime() << "Falha ao carregar a nuvem de pontos " << pointCloudInput << std::endl;
		return EXIT_FAILURE;
	}
	if (pointCloudFilter_tolerance >= 0)
	{
		std::cout << DateTime() << "Reduzindo o n�mero de pontos" << std::endl;
		std::cout << DateTime() << "N�mero de pontos atuais " << pointCloud->GetNumberOfPoints() << std::endl;
		// Filtrando a nuvem de pontos
		vtkNew<vtkVertexGlyphFilter> vertexFilter;
		vertexFilter->SetInputData(pointCloud);
		vertexFilter->Update();
		pointCloud = vertexFilter->GetOutput();
		vtkNew<vtkStaticCleanPolyData> cleanPolyData;
		cleanPolyData->SetInputData(pointCloud);
		cleanPolyData->SetTolerance(pointCloudFilter_tolerance / pointCloud->GetLength());
		cleanPolyData->Update();
		pointCloud = cleanPolyData->GetOutput();
		std::cout << DateTime() << "N�mero de pontos ap�s filtragem " << pointCloud->GetNumberOfPoints() << std::endl;
	}
	// Create the mesh with poisson
	std::cout << DateTime() << "Gerando a malha 3D e filtrando" << std::endl;
	auto mesh = HelperAdaptativeSolvers::ComputeAdaptativeSolvers(
		std::move(pointCloud),
		poissonParameters,
		surfaceTrimmerParameters);
	if (!mesh)
	{
		std::cout << DateTime() << "Falha ao gerar a mesh" << std::endl;
		return EXIT_FAILURE;
	}
	// Reduce the number of faces in the mesh
	std::cout << DateTime() << "Reduzindo o n�mero de faces" << std::endl;
	mesh = HelperFastQuadricSimplification::ComputeQuadricDecimation(
		std::move(mesh),
		fastQuadricSimplificationParameters);
	// Salvar a mesh
	std::cout << DateTime() << "Salvando a malha 3D" << std::endl;
	if (!PLYWriter::Write(meshOutput, mesh, true))
	{
		std::cout << DateTime() << "Erro ao salvar a mesh" << std::endl;
		return EXIT_FAILURE;
	}
	if (camerasInput == "") // We don't want to texturize the mesh
	{
		return EXIT_SUCCESS;
	}
	// Texturize the mesh
	std::cout << DateTime() << "Texturizando a malha 3D" << std::endl;
	if(HelperAVTexturing::computeAVTexturization(
		std::move(mesh),
		camerasInput,
		projectFolder,
		texturedMeshOutput,
		texturizationParameters)
		)
	{
		std::cout << DateTime() << "Erro na texturiza��o" << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}