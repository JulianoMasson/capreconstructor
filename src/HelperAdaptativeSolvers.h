#pragma once
#include <vtkSmartPointer.h>

#include <PoissonRecon.h>
#include <SurfaceTrimmer.h>

class vtkPolyData;

class HelperAdaptativeSolvers
{
public:
	HelperAdaptativeSolvers() {};
	~HelperAdaptativeSolvers() {};

	// Surface trimmer only works with triangle mesh, do not use options.polygonMesh
	// trim_value < 0 to disable surface trimmer
	static vtkSmartPointer<vtkPolyData> ComputeAdaptativeSolvers(
		vtkSmartPointer<vtkPolyData> polyData,
		const PoissonRecon::Options& poissonOptions,
		const SurfaceTrimmer::Options& surfaceTrimmerOptions);

private:
	template <typename T>
	static AdaptativeSolvers::Mesh<T> PolyData2AdaptativeSolvers(vtkSmartPointer<vtkPolyData> polyData, bool getFaces = false);

	template <typename T>
	static vtkSmartPointer<vtkPolyData> AdaptativeSolvers2polyData(const AdaptativeSolvers::Mesh<T>& mesh);
};


