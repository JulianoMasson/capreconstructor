#include "Utils.h"

#include <vector>
#include <sstream>
#include <Windows.h>

#include <vtkMath.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTransformFilter.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkTexture.h>
#include <vtkTransform.h>
#include <vtkImageImport.h>
#include <vtkImageData.h>
#include <vtkWindowToImageFilter.h>
#include <vtkImageReader2.h>
#include <vtkJPEGReader.h>
#include <vtkBMPReader.h>
#include <vtkTIFFReader.h>
#include <vtkPNGReader.h>
#include <vtkPNGWriter.h>
#include <vtkInteractorObserver.h>
#include <vtkAbstractPicker.h>
#include <vtkMatrix4x4.h>
#include <vtksys/SystemTools.hxx>
#include <vtkCleanPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkCenterOfMass.h>
#include <vtkFeatureEdges.h>
#include <vtkConnectivityFilter.h>

#include "Camera.h"

Utils::Utils()
{
}

Utils::~Utils()
{
}

void Utils::transformPoint(double point[3], vtkSmartPointer<vtkMatrix4x4> matrixRT)
{
	double x = (matrixRT->Element[0][0] * point[0] + matrixRT->Element[0][1] * point[1] + matrixRT->Element[0][2] * point[2] + matrixRT->Element[0][3]);
	double y = (matrixRT->Element[1][0] * point[0] + matrixRT->Element[1][1] * point[1] + matrixRT->Element[1][2] * point[2] + matrixRT->Element[1][3]);
	double z = (matrixRT->Element[2][0] * point[0] + matrixRT->Element[2][1] * point[1] + matrixRT->Element[2][2] * point[2] + matrixRT->Element[2][3]);
	point[0] = x; 
	point[1] = y; 
	point[2] = z;
}

bool Utils::exists(const std::string & name)
{
	std::ifstream infile(name);
	return infile.good();
}

std::string Utils::getFileExtension(const std::string & filePath)
{
	if (filePath.find_last_of('.') != std::string::npos)
	{
		return filePath.substr(filePath.find_last_of('.') + 1, filePath.size());
	}
	return "";
}

std::string Utils::getFileName(const std::string & filePath, bool withExtension)
{
	std::string fileName = filePath;
	if (fileName.find_last_of('/') != std::string::npos)
	{
		fileName = fileName.substr(fileName.find_last_of('/') + 1, fileName.size());
	}
	else if (filePath.find_last_of('\\') != std::string::npos)
	{
		fileName = fileName.substr(fileName.find_last_of('\\') + 1, fileName.size());
	}
	if (fileName != "" && !withExtension)
	{
		return fileName.substr(0, fileName.find_last_of('.'));
	}
	return fileName;
}

std::string Utils::getPath(const std::string & filePath, bool returnWithLastSlash)
{
	if (filePath.find_last_of('/') != std::string::npos)
	{
		if (returnWithLastSlash)
		{
			return filePath.substr(0, filePath.find_last_of('/') + 1);
		}
		else
		{
			return filePath.substr(0, filePath.find_last_of('/'));
		}
	}
	else if (filePath.find_last_of('\\') != std::string::npos)
	{
		if (returnWithLastSlash)
		{
			return filePath.substr(0, filePath.find_last_of('\\') + 1);
		}
		else
		{
			return filePath.substr(0, filePath.find_last_of('\\'));
		}
	}
	return "";
}

std::string Utils::toUpper(const std::string & str)
{
	std::string copy = str;
	for (auto & c : copy)
	{
		c = toupper(c);
	}
	return copy;
}
