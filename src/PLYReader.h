#pragma once

#include <vtkSmartPointer.h>

#include <string>

class vtkPolyData;

struct Property;
struct Header;

class PLYReader
{
public:
	PLYReader() {};
	~PLYReader() {};

	vtkSmartPointer<vtkPolyData> read(const std::string& filename);

private:
	void ignore_property(std::ifstream& in, const Property& prop);
	unsigned int find_type(const std::string& type);
	double get_item_value(std::ifstream& in, int type);
	bool readHeader(const std::string& filename, Header& plyHeader);
	bool isASCII = false;

};