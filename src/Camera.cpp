#include "Camera.h"

#include <sstream>

#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkMatrix4x4.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>

#include <vtkActor.h>
#include <vtkImageActor.h>
#include <vtkPolyDataMapper.h>

#include "ImageIO.h"
#include "Utils.h"

Camera::Camera()
{
}

Camera::Camera(const std::string & filePath, const float focalDistance[2], const float principalPoint[2],
	const unsigned int width, const unsigned int height, const vtkSmartPointer<vtkMatrix4x4> matrixRt) :
	filePath(filePath), focalDistance{ focalDistance[0], focalDistance[1] }, principalPoint{ principalPoint[0], principalPoint[1] },
	width(width), height(height)
{
	setMatrixRt(matrixRt);
	calcCameraPoints();
}

Camera::~Camera()
{
	for (int i = 0; i < cameraPoints.size(); i++)
	{
		delete cameraPoints.at(i);
	}
	cameraPoints.clear();
}

bool Camera::calcCameraPoints()
{
	if (width > 0 && height > 0 && matrixRtInverted)
	{
		for (int i = 0; i < cameraPoints.size(); i++)
		{
			delete cameraPoints.at(i);
		}
		cameraPoints.clear();
		double* p1 = new double[3];
		Utils::createDoubleVector(0, 0, 0, p1);
		Utils::transformPoint(p1, matrixRtInverted);
		double dist = 0.5;
		double minX, minY, maxX, maxY;
		maxX = dist * (width / (2.0 * focalDistance[0]));
		minX = -maxX;
		maxY = dist * (height / (2.0 * focalDistance[1]));
		minY = -maxY;
		/*
			origin of the camera = p1
			p2--------p3
			|		   |
			|  pCenter |<--- Looking from p1 to pCenter
			|          |
			p5--------p4
			*/
		double* p2 = new double[3];
		Utils::createDoubleVector(minX, minY, dist, p2);
		Utils::transformPoint(p2, matrixRtInverted);
		double* p3 = new double[3];
		Utils::createDoubleVector(maxX, minY, dist, p3);
		Utils::transformPoint(p3, matrixRtInverted);
		double* p4 = new double[3];
		Utils::createDoubleVector(maxX, maxY, dist, p4);
		Utils::transformPoint(p4, matrixRtInverted);
		double* p5 = new double[3];
		Utils::createDoubleVector(minX, maxY, dist, p5);
		Utils::transformPoint(p5, matrixRtInverted);
		double* pCenter = new double[3];
		Utils::createDoubleVector(0, 0, dist, pCenter);
		Utils::transformPoint(pCenter, matrixRtInverted);
		double* pUP = new double[3];
		Utils::createDoubleVector(0, maxY, dist*0.95, pUP);
		Utils::transformPoint(pUP, matrixRtInverted);
		cameraPoints.reserve(6);
		cameraPoints.emplace_back(p1);
		cameraPoints.emplace_back(p2);
		cameraPoints.emplace_back(p3);
		cameraPoints.emplace_back(p4);
		cameraPoints.emplace_back(p5);
		cameraPoints.emplace_back(pCenter);
		return 1;
	}
	return 0;
}

void Camera::updateFocalDistance(vtkSmartPointer<vtkRenderer> renderer, float focalX, float focalY)
{
	focalDistance[0] = focalX;
	focalDistance[1] = focalY;
}

void Camera::updateMatrixRt(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkMatrix4x4> matrixRt)
{
	if (!this->matrixRt)
	{
		this->matrixRt = vtkSmartPointer<vtkMatrix4x4>::New();
		matrixRtInverted = vtkSmartPointer<vtkMatrix4x4>::New();
	}
	this->matrixRt->DeepCopy(matrixRt);
	matrixRt->Invert();
	this->matrixRtInverted->DeepCopy(matrixRt);
}

void Camera::setMatrixRt(vtkSmartPointer<vtkMatrix4x4> matrixRt)
{
	if (!this->matrixRt)
	{
		this->matrixRt = vtkSmartPointer<vtkMatrix4x4>::New();
		matrixRtInverted = vtkSmartPointer<vtkMatrix4x4>::New();
	}
	this->matrixRt->DeepCopy(matrixRt);
	matrixRt->Invert();
	this->matrixRtInverted->DeepCopy(matrixRt);
}

vtkSmartPointer<vtkMatrix4x4> Camera::getMatrixRt() const
{
	if (matrixRt)
	{
		vtkNew<vtkMatrix4x4> temp;
		temp->DeepCopy(matrixRt);
		return temp;
	}
	return nullptr;
}

vtkSmartPointer<vtkPolyData> Camera::getClosedFrustum(double dist)
{
	if (!calcCameraPoints())
	{
		return nullptr;
	}
	double minX, minY, maxX, maxY;
	maxX = dist * (width / (2.0 * focalDistance[0]));
	minX = -maxX;
	maxY = dist * (height / (2.0 * focalDistance[1]));
	minY = -maxY;
	double p1[3] = { 0, 0, 0 };
	Utils::transformPoint(p1, matrixRtInverted);
	double p2[3] = {minX, minY, dist}; 
	Utils::transformPoint(p2, matrixRtInverted);
	double p3[3] = {maxX, minY, dist}; 
	Utils::transformPoint(p3, matrixRtInverted);
	double p4[3] = {maxX, maxY, dist}; 
	Utils::transformPoint(p4, matrixRtInverted);
	double p5[3] = {minX, maxY, dist}; 
	Utils::transformPoint(p5, matrixRtInverted);
	/*
	origin of the camera = p1
	p2--------p3
	|		   |
	|          |
	|          |
	p5--------p4
	*/
	vtkNew<vtkPoints> frustumPoints;
	frustumPoints->InsertNextPoint(p1);//0
	frustumPoints->InsertNextPoint(p2);//1
	frustumPoints->InsertNextPoint(p3);//2
	frustumPoints->InsertNextPoint(p4);//3
	frustumPoints->InsertNextPoint(p5);//4

	vtkNew<vtkCellArray> frustumPolys;
	vtkNew<vtkTriangle> tri;
	tri->GetPointIds()->SetId(0, 0);
	tri->GetPointIds()->SetId(1, 1);
	tri->GetPointIds()->SetId(2, 4);
	frustumPolys->InsertNextCell(tri);
	vtkNew<vtkTriangle> tri2;
	tri2->GetPointIds()->SetId(0, 0);
	tri2->GetPointIds()->SetId(1, 2);
	tri2->GetPointIds()->SetId(2, 1);
	frustumPolys->InsertNextCell(tri2);
	vtkNew<vtkTriangle> tri3;
	tri3->GetPointIds()->SetId(0, 0);
	tri3->GetPointIds()->SetId(1, 2);
	tri3->GetPointIds()->SetId(2, 3);
	frustumPolys->InsertNextCell(tri3);
	vtkNew<vtkTriangle> tri4;
	tri4->GetPointIds()->SetId(0, 0);
	tri4->GetPointIds()->SetId(1, 3);
	tri4->GetPointIds()->SetId(2, 4);
	frustumPolys->InsertNextCell(tri4);
	vtkNew<vtkTriangle> tri5;
	tri5->GetPointIds()->SetId(0, 1);
	tri5->GetPointIds()->SetId(1, 2);
	tri5->GetPointIds()->SetId(2, 3);
	frustumPolys->InsertNextCell(tri5);
	vtkNew<vtkTriangle> tri6;
	tri6->GetPointIds()->SetId(0, 1);
	tri6->GetPointIds()->SetId(1, 3);
	tri6->GetPointIds()->SetId(2, 4);
	frustumPolys->InsertNextCell(tri6);

	vtkNew<vtkPolyData> frustumPolyData;
	frustumPolyData->SetPoints(frustumPoints);
	frustumPolyData->SetPolys(frustumPolys);
	return frustumPolyData;
}
