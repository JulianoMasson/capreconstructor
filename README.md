# README #

### How do I get set up? ###

Use Visual Studio 2017 or newer and CMake >= 3.14.

Libraries  
1 - VTK >= 9.0.1 - You need to build it adding the vtkSMPTools (with TBB).  
2 - AdaptativeSolvers - You can download it here https://github.com/julianomasson/PoissonRecon/releases , used to create a poisson/SSD reconstruction and the surface trimmer.  
3 - Claudette - Included in the repository, used to create point visibility.  
4 - AVTexturing (AliceVision texturing) - Included in the repository, used to texturize the mesh.  
5 - FastQuadricMeshSimplification - You can download it here: https://github.com/julianomasson/Fast-Quadric-Mesh-Simplification  

The compiled AVTexturing is available in the Downloads section.    

### Using the CAPReconstructor ###

#### Changing the parameters ####

You can disable the surface trimmer setting the trim_value < 0 and the point cloud filter setting the tolerance < 0  

#### Generating a mesh ####

**Command line**: .\CAPReconstructor.exe <parameters_file> <point_cloud_input_path> <mesh_output_path>

**Example**:  .\CAPReconstructor.exe .\parameters.json .\project\pointCloud.ply .\project\mesh.ply

#### Generating a textured mesh ####

**Command line**: .\CAPReconstructor.exe <parameters_file> <point_cloud_input_path> <mesh_output_path> <cameras_sfm/nvm_path> <project_folder> <textured_mesh_output_path>

**Example**:  .\CAPReconstructor.exe .\parameters.json .\project\pointCloud.ply .\project\mesh.ply .\project\cameras.sfm .\project\ .\project\textured\texturedMesh.obj  

PATH=C:\libs\VTK-9.0.3\build\bin\Release;C:\libs\AliceVision\Windows-AMD64\Release;C:\libs\AdaptativeSolvers\bin\Release;C:\libs\FastQuadricMeshSimplification\lib;C:\libs\Claudette\install\lib;C:\libs\TBB-2020.3.0\bin\intel64\vc14;%PATH%  

PATH=C:\libs\VTK-9.0.3\build\bin\Debug;C:\libs\AliceVision\Windows-AMD64\Debug;C:\libs\AdaptativeSolvers\bin\Debug;C:\libs\FastQuadricMeshSimplification\lib;C:\libs\Claudette\install\lib;C:\libs\TBB-2020.3.0\bin\intel64\vc14;%PATH%  
