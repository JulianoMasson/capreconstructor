cmake_minimum_required(VERSION 3.14)
project(CAPReconstructor)

add_definitions(/openmp)

#VTK
find_package(VTK REQUIRED)

#AdaptativeSolvers
find_path(ADAPTATIVESOLVERS_INSTALL_DIR bin/Release/PoissonRecon.dll REQUIRED)
if (ADAPTATIVESOLVERS_INSTALL_DIR)
    include_directories("${ADAPTATIVESOLVERS_INSTALL_DIR}/include")
endif ()

#FastQuadricSimplification
find_path(FASTQUADRICSIMP_INCLUDE_DIR FastQuadricMeshSimplification.h REQUIRED)
if (FASTQUADRICSIMP_INCLUDE_DIR)
    include_directories(${FASTQUADRICSIMP_INCLUDE_DIR})
endif ()
find_path(FASTQUADRICSIMP_LIB_DIR FastQuadricMeshSimplification.dll REQUIRED)

#Claudette
find_path(CLAUDETTE_INSTALL_DIR "lib/claudette.dll" REQUIRED)
if (CLAUDETTE_INSTALL_DIR)
    include_directories("${CLAUDETTE_INSTALL_DIR}/include")
endif ()

#AVTexturing
find_path(AVTEXTURING_INCLUDE_DIR AVTexturing.h REQUIRED)
find_path(AVTEXTURING_LIB_RELEASE_DIR AVTexturing.dll REQUIRED)
find_path(AVTEXTURING_LIB_DEBUG_DIR AVTexturing.dll REQUIRED)
if (AVTEXTURING_INCLUDE_DIR)
    include_directories(${AVTEXTURING_INCLUDE_DIR})
endif ()

#OpenGL
find_package(OpenGL)
include_directories(${OPENGL_INCLUDE_DIRS})

#TBB
find_path(TBB_DLL_RELEASE "tbb.dll" REQUIRED)


add_executable(${PROJECT_NAME}  src/CAPReconstructor.cpp
				src/PLYReader.cpp
				src/PLYReader.h
				src/PLYWriter.cpp
				src/PLYWriter.h
				src/HelperAdaptativeSolvers.cpp
				src/HelperAdaptativeSolvers.h
				src/HelperFastQuadricSimplification.cpp
				src/HelperFastQuadricSimplification.h
				src/HelperAVTexturing.cpp
				src/HelperAVTexturing.h
				src/Camera.cpp
				src/Camera.h
				src/ImageIO.cpp
				src/ImageIO.h
				src/Utils.cpp
				src/Utils.h
				src/json.hpp)

target_link_libraries(${PROJECT_NAME} ${VTK_LIBRARIES})

target_link_libraries(${PROJECT_NAME} debug "${ADAPTATIVESOLVERS_INSTALL_DIR}/lib/Debug/*.lib" 
                                      optimized "${ADAPTATIVESOLVERS_INSTALL_DIR}/lib/Release/*.lib")

target_link_libraries(${PROJECT_NAME} debug "${FASTQUADRICSIMP_LIB_DIR}/FastQuadricMeshSimplificationd.lib" 
                                      optimized "${FASTQUADRICSIMP_LIB_DIR}/FastQuadricMeshSimplification.lib")

target_link_libraries(${PROJECT_NAME} "${CLAUDETTE_INSTALL_DIR}/lib/claudette.lib")

target_link_libraries(${PROJECT_NAME} debug "${AVTEXTURING_LIB_RELEASE_DIR}/AVTexturing.lib" 
                                      optimized "${AVTEXTURING_LIB_DEBUG_DIR}/AVTexturing.lib")

target_link_libraries(${PROJECT_NAME} ${OPENGL_LIBRARIES})

add_definitions(-DNOMINMAX
		-D_SCR_SECURE_NO_WARNINGS
		-D_CRT_SECURE_NO_WARNINGS)

#Version control
set (CAPRECONSTRUCTOR_VERSION_MAJOR "1")
set (CAPRECONSTRUCTOR_VERSION_MINOR "1")
set (CAPRECONSTRUCTOR_VERSION_PATCH "0")


add_definitions(-DCAPRECONSTRUCTOR_VERSION_MAJOR=${CAPRECONSTRUCTOR_VERSION_MAJOR}
				-DCAPRECONSTRUCTOR_VERSION_MINOR=${CAPRECONSTRUCTOR_VERSION_MINOR}
				-DCAPRECONSTRUCTOR_VERSION_PATCH=${CAPRECONSTRUCTOR_VERSION_PATCH})